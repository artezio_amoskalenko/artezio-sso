package com.artezio.alyonamoskalenko.Service;


import com.artezio.alyonamoskalenko.Entity.Session;
import com.artezio.alyonamoskalenko.Entity.User;
import com.artezio.alyonamoskalenko.RepositoryDAO.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Base64;
import java.util.List;

@Service
public class SessionService {
    private static final SecureRandom secureRandom = new SecureRandom(); //threadsafe
    private static final Base64.Encoder base64Encoder = Base64.getUrlEncoder(); //threadsafe

    @Autowired
    SessionRepository sessionRepository;
  //  @PersistenceContext
 //   private EntityManager em;
    public List<Session> getSessionList() {
        return (List<Session>) sessionRepository.findAll();
    }

    public Session getSessionById(Long id) {
        return sessionRepository.findById(id).get();
    }

    public void saveOrUpdateSession(Session session) {
      //  session.setToken(generateNewToken());
        sessionRepository.save(session);
    }

    public void deleteSession(Long id) {
        sessionRepository.deleteById(id);
    }

    public void deleteSession(Session session) {
        sessionRepository.delete(session);
    }
    public Session getSessionByToken(String token) {
        return sessionRepository.findByToken(token);
    }

    public static String generateNewToken() {
        byte[] randomBytes = new byte[24];
        secureRandom.nextBytes(randomBytes);
        return base64Encoder.encodeToString(randomBytes);
    }
}
