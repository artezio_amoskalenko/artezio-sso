package com.artezio.alyonamoskalenko.Service;
import com.artezio.alyonamoskalenko.Entity.Session;
import com.artezio.alyonamoskalenko.Entity.User;
import com.artezio.alyonamoskalenko.RepositoryDAO.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.List;

@Service
public class UserService   {

    @Autowired
    UserRepository userRepository;

  //  @PersistenceContext
 //   private EntityManager em;



    public List<User> getUserList() {
        return (List<User>) userRepository.findAll();
    }

    public User getUserById(Long id) {
        return userRepository.findById(id).get();
    }
    public User getUserByLogin(String login) {
        return userRepository.findByLogin(login);
    }
    public void updateUser(User user) {

        userRepository.save(user);

    }
    public void updateUserMD5Pas(User user) {
        user.setPassword( toMD5(user.getPassword()));
        userRepository.save(user);

    }
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    public void deleteUser(User user) {
        userRepository.delete(user);
    }

    public boolean saveUser(User user) {
        User userFromDB = userRepository.findByLogin(user.getLogin());

        if (userFromDB != null) {
            return false;
        }

        user.setPassword( toMD5(user.getPassword()));

        userRepository.save(user);
        return true;
    }
    public User login(User user) {

        User userFromDB = userRepository.findByLogin(user.getLogin());


        if (userFromDB != null) {
            if( userFromDB.getPassword().equals(toMD5(user.getPassword()))){

                   return  userFromDB;
            };

        }
      return null;
    }

    String toMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("md5");
            byte[] raw = md.digest(input.getBytes());
            return DatatypeConverter.printHexBinary(raw);
        }

         catch (NoSuchAlgorithmException e) {
            System.err.println("I'm sorry, but MD5 is not a valid message digest algorithm");
        }
        return input;
    }
}
