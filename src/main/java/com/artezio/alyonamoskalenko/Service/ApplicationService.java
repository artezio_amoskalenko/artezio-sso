package com.artezio.alyonamoskalenko.Service;

import com.artezio.alyonamoskalenko.Entity.Application;

import com.artezio.alyonamoskalenko.RepositoryDAO.ApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ApplicationService {

    @Autowired
    ApplicationRepository applicationRepository;
  //  @PersistenceContext
 //   private EntityManager em;
    public List<Application> getApplicationList() {
        return (List<Application>) applicationRepository.findAll();
    }

    public Application getApplicationById(Long id) {
        return applicationRepository.findById(id).get();
    }
    public void saveOrUpdateApplication(Application application) {
        applicationRepository.save(application);
    }

    public void deleteApplication(Long id) {
        applicationRepository.deleteById(id);
    }
    public Application  getApplicationByDomain(String domain){return applicationRepository.findByDomain(domain);}
    public void deleteApplication(Application application) {
        applicationRepository.delete(application);
    }
}
