package com.artezio.alyonamoskalenko.Service;

import com.artezio.alyonamoskalenko.Entity.Application;
import com.artezio.alyonamoskalenko.Entity.Role;
import com.artezio.alyonamoskalenko.Entity.User;
import com.artezio.alyonamoskalenko.RepositoryDAO.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class RoleService {

    @Autowired
    RoleRepository roleRepository;
    @PersistenceContext
    private EntityManager em;
    public List<Role> getRoleList() {
        return (List<Role>) roleRepository.findAll();
    }

    public Role getRoleById(Long id) {
        return roleRepository.findById(id).get();
    }
    public void saveOrUpdateRole(Role role) {
        roleRepository.save(role);
    }
    public void deleteRole(Long id) {
        roleRepository.deleteById(id);
    }
    public void deleteRole(Role role) {
        roleRepository.delete(role);
    }

    public List<Role> getRolesByUserApp(Long idUsr, Long idApp) {
        return em.createNativeQuery("SELECT r.* FROM Roles r, usersrolesapps u WHERE u.id_role=r.id and u.id_user = "+idUsr+" and u.id_app = "+idApp, Role.class)
                .getResultList();
    }
}
