package com.artezio.alyonamoskalenko.Controller;

import com.artezio.alyonamoskalenko.Entity.Application;
import com.artezio.alyonamoskalenko.Service.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class ApplicationRestController {

    @Autowired
    private ApplicationService applicationService;

    @GetMapping("/api/applicationList")
    public ResponseEntity<List<Application>> getApplicationList() {
        return new ResponseEntity<List<Application>>(applicationService.getApplicationList(), HttpStatus.OK);
    }

    @GetMapping("/api/application/{id}")
    public ResponseEntity<Application> getApplication(@PathVariable Long id) {
        return new ResponseEntity<Application>(applicationService.getApplicationById(id), HttpStatus.OK);
    }

    @PostMapping("/api/application")
    public ResponseEntity<Void> addApplication(@RequestBody Application application) {
        applicationService.saveOrUpdateApplication(application);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    @PutMapping("/api/application")
    public ResponseEntity<Void> updateApplication(@RequestBody Application application) {
        applicationService.saveOrUpdateApplication(application);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    @DeleteMapping("/api/application/{id}")
    public ResponseEntity<Void> deleteApplication(@PathVariable Long id) {
        applicationService.deleteApplication(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }



}