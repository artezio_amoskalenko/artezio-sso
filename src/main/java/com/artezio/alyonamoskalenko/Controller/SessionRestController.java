package com.artezio.alyonamoskalenko.Controller;

import com.artezio.alyonamoskalenko.Entity.Session;
import com.artezio.alyonamoskalenko.Service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class SessionRestController {

    @Autowired
    private SessionService sessionService;

    @GetMapping("/api/sessionList")
    public ResponseEntity<List<Session>> getSessionList() {
        return new ResponseEntity<List<Session>>(sessionService.getSessionList(), HttpStatus.OK);
    }

    @GetMapping("/api/session/{id}")
    public ResponseEntity<Session> getSession(@PathVariable Long id) {
        return new ResponseEntity<Session>(sessionService.getSessionById(id), HttpStatus.OK);
    }

    @PostMapping("/api/session")
    public ResponseEntity<Void> addSession(@RequestBody Session session) {
        sessionService.saveOrUpdateSession(session);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    @PutMapping("/api/session")
    public ResponseEntity<Void> updateSession(@RequestBody Session session) {
        sessionService.saveOrUpdateSession(session);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    @DeleteMapping("/api/session/{id}")
    public ResponseEntity<Void> deleteSession(@PathVariable Long id) {
        sessionService.deleteSession(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}