package com.artezio.alyonamoskalenko.Controller;

import com.artezio.alyonamoskalenko.Entity.Application;
import com.artezio.alyonamoskalenko.Entity.Session;
import com.artezio.alyonamoskalenko.Entity.User;
import com.artezio.alyonamoskalenko.Service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.beans.binding.BooleanBinding;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
public class UserRestController {

    @Autowired
    private UserService userService;

    @GetMapping("/api/userList")
    public ResponseEntity<List<User>> getUserList() {
        return new ResponseEntity<List<User>>(userService.getUserList(), HttpStatus.OK);
    }

    @GetMapping("/api/user/{id}")
    public ResponseEntity<User> getUser(@PathVariable Long id) {
        return new ResponseEntity<User>(userService.getUserById(id), HttpStatus.OK);
    }

    @PostMapping("/api/user")
    public ResponseEntity<String> addUser(@RequestBody User user) {
        if (!userService.saveUser(user)){
            return new ResponseEntity<String>("Пользователь с таким именем уже существует", HttpStatus.OK);
        }

        return new ResponseEntity<String>("OK", HttpStatus.OK);
    }
    @PutMapping("/api/user")
    public ResponseEntity<String> updateUser(@RequestBody String json) {




        ObjectMapper mapper = new ObjectMapper();

        try {   Map<String, String> requestParams  = mapper.readValue(json, new TypeReference<Map<String,String>>(){});

            User user = userService.getUserById(Long.parseLong(requestParams.get("id")));
            User usr = userService.getUserByLogin(requestParams.get("login"));
            if (usr!=null)
                if (usr.getId()!=user.getId()) {
                    return new ResponseEntity<String>("Пользователь с таким именем уже существует", HttpStatus.OK);
                }
            user.setLogin(requestParams.get("login"));
            user.setPassword(requestParams.get("password"));
            if (requestParams.get("check") != null)
            if (requestParams.get("check").equals("1")){
                userService.updateUserMD5Pas(user);
                return new ResponseEntity<String>( mapper.writeValueAsString(user), HttpStatus.OK);
            }

                userService.updateUser(user);

            return new ResponseEntity<String>(  mapper.writeValueAsString(user),HttpStatus.OK);

        }

        catch (JsonMappingException e) {
            return new  ResponseEntity<String>("JsonMappingException" , HttpStatus.BAD_REQUEST);
        }
        catch (JsonProcessingException e) {
            return new  ResponseEntity<String>( "JsonProcessingException", HttpStatus.BAD_REQUEST);
        }
    }
    @DeleteMapping("/api/user/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }



}