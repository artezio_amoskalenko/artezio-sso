package com.artezio.alyonamoskalenko.Controller;

import com.artezio.alyonamoskalenko.Entity.Role;
import com.artezio.alyonamoskalenko.Service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class RoleRestController {

    @Autowired
    private RoleService roleService;

    @GetMapping("/api/roleList")
    public ResponseEntity<List<Role>> getRoleList() {
        return new ResponseEntity<List<Role>>(roleService.getRoleList(), HttpStatus.OK);
    }

    @GetMapping("/api/role/{id}")
    public ResponseEntity<Role> getRole(@PathVariable Long id) {
        return new ResponseEntity<Role>(roleService.getRoleById(id), HttpStatus.OK);
    }

    @PostMapping("/api/role")
    public ResponseEntity<Void> addRole(@RequestBody Role role) {
        roleService.saveOrUpdateRole(role);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    @PutMapping("/api/role")
    public ResponseEntity<Void> updateRole(@RequestBody Role role) {
        roleService.saveOrUpdateRole(role);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    @DeleteMapping("/api/role/{id}")
    public ResponseEntity<Void> deleteRole(@PathVariable Long id) {
        roleService.deleteRole(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }



}