package com.artezio.alyonamoskalenko.Controller;

import com.artezio.alyonamoskalenko.Entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

@Controller
public class HomeController {

   @GetMapping("/")
    public String home() {
        return "index";
    }
    @GetMapping("/admin")
    public String admin() {

        return "admin";
    }


}