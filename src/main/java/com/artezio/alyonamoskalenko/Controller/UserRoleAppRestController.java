package com.artezio.alyonamoskalenko.Controller;

import com.artezio.alyonamoskalenko.Entity.UserRoleApp;
import com.artezio.alyonamoskalenko.Service.UserRoleAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class UserRoleAppRestController {

    @Autowired
    private UserRoleAppService userRoleAppService;

    @GetMapping("/api/userRoleAppList")
    public ResponseEntity<List<UserRoleApp>> getUserRoleAppList() {
        return new ResponseEntity<List<UserRoleApp>>(userRoleAppService.getUserRoleAppList(), HttpStatus.OK);
    }




}