package com.artezio.alyonamoskalenko.Controller;

import com.artezio.alyonamoskalenko.Entity.Application;
import com.artezio.alyonamoskalenko.Entity.User;
import com.artezio.alyonamoskalenko.Entity.Session;
import com.artezio.alyonamoskalenko.Service.ApplicationService;
import com.artezio.alyonamoskalenko.Service.RoleService;
import com.artezio.alyonamoskalenko.Service.UserService;

import com.artezio.alyonamoskalenko.Service.SessionService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
public class LoginRestController {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private SessionService sessionService;
    @Autowired
    private ApplicationService applicationService;
    @Autowired
    private HttpSession httpSession;

    @PostMapping("/api/login")
    public ResponseEntity<String> loginUser(@RequestBody User user ) {

        User usr = userService.login(user);
        if(usr!=null){
            Session session = usr.getSession();
            if (session == null)
                session = new Session();
            session.setId(usr.getId());
            session.setToken(SessionService.generateNewToken());
            session.setUser(usr);
            Timestamp timestamp = new Timestamp(System.currentTimeMillis() + 3* 24 * 60 * 60 * 1000);
            session.setTtl(timestamp);
            usr.setSession(session);
           // session.setUser(usr);

            userService.updateUser(usr);
            //return new ResponseEntity<Session>(session, HttpStatus.OK);


                httpSession.setAttribute("session",session);

            return new ResponseEntity<String>(session.toString(), HttpStatus.OK);
        }
           // return new ResponseEntity<String>(userService.login(user).toString(), HttpStatus.OK);

        return new ResponseEntity<String>("Логин или пароль введены не верно", HttpStatus.OK);


    }

    @PostMapping("/api/logintoken")
    public ResponseEntity<String> loginToken(@RequestBody String json){

        ObjectMapper mapper = new ObjectMapper();

        try {   Map<String, String> requestParams  = mapper.readValue(json, new TypeReference<Map<String,String>>(){});

           Session session =  sessionService.getSessionByToken(requestParams.get("token"));
           Application app =  applicationService.getApplicationByDomain(requestParams.get("app"));
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
           if  (session.getTtl().after(timestamp)) {

               return new ResponseEntity<String>(roleService.getRolesByUserApp(session.getId(), app.getId()).toString(), HttpStatus.OK);
           }
            return new ResponseEntity<String>("Сессия истекла", HttpStatus.OK);

        }
        catch (JsonMappingException e) {
            return new  ResponseEntity<String>("JsonMappingException" , HttpStatus.BAD_REQUEST);
        }
        catch (JsonProcessingException e) {
            return new  ResponseEntity<String>( "JsonProcessingException", HttpStatus.BAD_REQUEST);
        }
    }

}