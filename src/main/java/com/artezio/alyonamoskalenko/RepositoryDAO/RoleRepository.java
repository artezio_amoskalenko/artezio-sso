package com.artezio.alyonamoskalenko.RepositoryDAO;
import com.artezio.alyonamoskalenko.Entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RoleRepository extends JpaRepository<Role, Long> {

}