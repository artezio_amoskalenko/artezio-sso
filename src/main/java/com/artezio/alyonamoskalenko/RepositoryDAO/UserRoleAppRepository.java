package com.artezio.alyonamoskalenko.RepositoryDAO;

import com.artezio.alyonamoskalenko.Entity.UserRoleApp;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface UserRoleAppRepository  //extends Repository<UserRoleApp, Long>
{
    List<UserRoleApp> getUserRoleAppList();
}
