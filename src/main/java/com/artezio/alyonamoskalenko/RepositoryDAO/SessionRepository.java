package com.artezio.alyonamoskalenko.RepositoryDAO;
import com.artezio.alyonamoskalenko.Entity.Session;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface SessionRepository extends JpaRepository<Session, Long> {

    Session findByToken(String token);
}