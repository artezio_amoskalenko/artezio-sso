package com.artezio.alyonamoskalenko.RepositoryDAO;
import com.artezio.alyonamoskalenko.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User, Long> {
    User findByLogin(String login);
}