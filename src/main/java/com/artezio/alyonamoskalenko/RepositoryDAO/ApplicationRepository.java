package com.artezio.alyonamoskalenko.RepositoryDAO;
import com.artezio.alyonamoskalenko.Entity.Application;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ApplicationRepository extends JpaRepository<Application, Long> {
    Application findByDomain(String domain);
}