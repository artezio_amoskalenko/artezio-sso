package  com.artezio.alyonamoskalenko.Entity;

import javax.persistence.*;


@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //GenerationType.IDENTITY - прописать auto increment в базе Mysql
    @Column(name = "id")
    private Long id;
    @Column(name = "login", length = 80)
    private String login;
    @Column(name = "password",  length = 255)
    private String password;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "user")

    private Session session;
/*@JoinTable(name = "usersrolesapps",
            joinColumns = @JoinColumn(name = "id_user"),
            inverseJoinColumns = {
                     @JoinColumn(name = "id_app")
            })
    @MapKeyJoinColumn(name = "id_role")
    @ElementCollection
private Map<Role, Application> roleApp = new HashMap<>();
*/
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setSession(Session session) {
        this.session = session;
    }
    public Session getSession() {
        return session;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +

                '}';
    }
}
