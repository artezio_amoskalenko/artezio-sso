package com.artezio.alyonamoskalenko.Entity;

import javax.persistence.*;

import com.artezio.alyonamoskalenko.Entity.Application;
import com.artezio.alyonamoskalenko.Entity.Role;
import com.artezio.alyonamoskalenko.Entity.User;

import java.io.Serializable;

@Embeddable
@Table(name = "usersrolesapps")
public class UserRoleApp{
    @ManyToOne(targetEntity = User.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "id_user", referencedColumnName = "id")
    private User user;

    @ManyToOne(targetEntity = Role.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn( name = "id_role", referencedColumnName = "id")
    private Role role;

    @ManyToOne(targetEntity = Application.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "id_app", referencedColumnName = "id")
    private Application application;

   public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

  public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }
}
