package  com.artezio.alyonamoskalenko.Entity;

import javax.persistence.*;

@Entity
@Table(name = "applications")
public class Application {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //GenerationType.IDENTITY - прописать auto increment в базе Mysql
    @Column(name = "id")
    private Long id;
    @Column(name = "domain", length = 100)
    private String domain;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getDomain() {
        return domain;
    }
    public void setDomain(String domain) {
        this.domain = domain;
    }


}
