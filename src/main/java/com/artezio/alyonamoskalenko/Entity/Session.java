package  com.artezio.alyonamoskalenko.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

@Entity
@Table(name = "sessions" )
public class Session {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //GenerationType.IDENTITY - прописать auto increment в базе Mysql
    @Column(name = "id")
    private Long id;
    @Column(name = "token", length = 255)
    private String token;
    @Column(name = "ttl")
    @JsonFormat(pattern="dd.MM.YYYY HH:mm:ss",  timezone="Europe/Minsk")

    private Timestamp ttl;

    @MapsId
    @OneToOne
    @JoinColumn(name = "id")
    private User user;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }


    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
     public Timestamp getTtl() {
        return ttl;
    }
    public void setTtl(Timestamp ttl) {
        this.ttl = ttl;
    }
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Session{" +
                "id=" + id +
                ", token='" + token + '\'' +
                ", ttl=" + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(ttl) +

                '}';
    }
}
