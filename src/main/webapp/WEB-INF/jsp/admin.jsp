<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!doctype html>
<html>
<head>
<%@ include file="parts/meta.jsp" %>
<title>Главная</title>
<%@ include file="parts/head.jsp" %>
</head>
<body>
<%@ include file="parts/header.jsp" %>
<div class="container-fluid">
    <div class="row">
        <div class="col-auto">
            <ul class="list-group">
                <li class="list-group-item"><a href="#" class="admin-users">Пользователи</a></li>
                <li class="list-group-item"><a href="#" class="admin-applications">Приложения</a></li>
                <li class="list-group-item"><a href="#" class="admin-roles">Роли</a></li>
                <li class="list-group-item"><a href="#" class="admin-userroleapp">Связи</a></li>
            </ul>
        </div>
        <div class="col" id="right_admin">
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="add" action="#" class="" >
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                        </div>
                        <div class="modal-footer">
                            <div id='msg'></div>
                            <button type="submit" class="btn btn-primary" id="addNew">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
    function getUsers() {
        $.getJSON('http://localhost:8080/api/userList', function (json) {
            var tr = [];
            for (var i = 0; i < json.length; i++) {
                tr.push('<tr>');
                tr.push('<td><p>' + json[i].id + '</p></td>');
                tr.push('<td><p>' + json[i].login + '</p></td>');
                tr.push('<td><p>' + json[i].password + '</p></td>');
                tr.push('<td>');

                if (json[i].session!=null) {


                        tr.push('<p data-id="' + json[i].session.id + '">' + json[i].session.token + "<br>"+ json[i].session.ttl + '</p>');

                };
                tr.push('</td>');

                tr.push('<td><button type="button" class="btn btn-success edit user">Редактировать</button>&nbsp;&nbsp;<button type="button" class="btn btn-danger delete user" id=' + json[i].id + '>Удалить</button></td>');
                tr.push('</tr>');
            }
            ;
            $('#right_admin').html('<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Добавить нового пользователя</button> <table class="table shadow "><thead class="thead-dark"><tr><th  class="w-25">Id</th><th  class="w-25">Имя пользователя</th><th  class="w-25">Пароль</th><th  class="w-25">Сессия</th><th  class="w-25">Действия</th></tr></thead><tbody>'+tr.join('')+'</tbody></table>');

            $('.modal-body').html('<div class="form-group"> <label for="login">Имя пользователя</label><input type="text" class="form-control" id="login" name="login"></div>');

            $('.modal-body').append('<div class="form-group"> <label for="password">Пароль</label><input type="text" class="form-control" id="password" name="password"></div>');

            $('.modal-title').html('Добавление пользователя');
        });
        $('#addNew').removeClass();
        $('#addNew').addClass('btn');
        $('#addNew').addClass('btn-primary');
        $('#addNew').addClass('btn-user');
    };
    $(document).delegate('.admin-users', 'click', function(event) {
        event.preventDefault();
        getUsers();
    });
    $(document).delegate('#addNew.btn-user', 'click', function(event) {
        event.preventDefault();

        var name = $('#login').val();
        var password = $('#password').val();

        var json = {
            "login" : name,
            "password":password
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "http://localhost:8080/api/user",
            data: JSON.stringify(json),
            cache: false,
            success: function(data) {
                if (data=="OK")
                {
                    $("#msg").html( "<span style='color: green'>Пользователь успешно добавлен</span>" );
                    window.setTimeout(function(){getUsers(); $("#msg").html()},1000)


                } else
                {   $("#msg").html( "<span style='color: red'>"+data+"</span>" );
                    window.setTimeout(function(){ $("#msg").html()},1000);
                }
            },
            error: function(err) {
                $("#msg").html( "<span style='color: red'>Что-то пошло не так</span>" );
                window.setTimeout(function(){ $("#msg").html()},1000);
            }
        });
    });
    $(document).delegate('.edit.user', 'click', function() {
        var parent = $(this).parent().parent();
        var id = parent.children("td:nth-child(1)").children("p");
        var login = parent.children("td:nth-child(2)");
        var password = parent.children("td:nth-child(3)");
        var buttons = parent.children("td:nth-child(5)");

        login.html("<input type='text' class='form-control'   value='" + login.children("p").html() + "'/>");
        password.html("<input type='text' class='form-control'  id='pass'  value='" + password.children("p").html() + "'/> <input type='checkbox' id='passMd5' name='passMd5' value='1' checked>Шифровать пароль</input>");

        buttons.html('<button type="button" class="btn btn-success save user" ">Сохранить</button>&nbsp;&nbsp;<button type="button" class="btn btn-danger delete user" id=' + id.html() + '>Удалить</button>');
    });
    $(document).delegate('.delete.user', 'click', function() {
        if (confirm('Вы действительно хотите удалить запись?')) {
            var id = $(this).attr('id');
            var parent = $(this).parent().parent();
            $.ajax({
                type: "DELETE",
                url: "http://localhost:8080/api/user/" + id,
                cache: false,
                success: function() {
                    parent.fadeOut('slow', function() {
                        $(this).remove();
                    });
                    getUsers()
                },
                error: function() {
                    $('#err').html('<span style=\'color:red; font-weight: bold; font-size: 30px;\'>Ошибка удаления записи').fadeIn().fadeOut(4000, function() {
                        $(this).remove();
                    });
                }
            });
        }
    });

    $(document).delegate('.save.user', 'click', function() {
        var parent = $(this).parent().parent();

        var id = parent.children("td:nth-child(1)").children("p");
        var login = parent.children("td:nth-child(2)");
        var password = parent.children("td:nth-child(3)");
        var buttons = parent.children("td:nth-child(5)");
       var check =  password.children("input[type='checkbox']:checked");
       if (check !=null){check= check.val();}
       else {check=0;}
        $.ajax({
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            url: "http://localhost:8080/api/user",
            data: JSON.stringify({'id' : id.html(), 'login' : login.children("input[type=text]").val(), 'password' : password.children("#pass").val(),'check' : check
            }),
            cache: false,
            success: function(data) {
                try {
                    json= JSON.parse(data);
                    login.html('<p>'+json.login+'</p>');
                    password.html('<p>'+json.password+'</p>');


                } catch (e) {
                    alert(data);

                }
                       buttons.html('<button type="button" class="btn btn-success edit user" id="' + id.html() + '">Редактировать</button>&nbsp;&nbsp;<button type="button" class="btn btn-danger delete user" id=' + id.html() + '>Удалить</button>');
            },
            error: function() {
                $('#err').html('<span style="color:red; font-weight: bold; font-size: 30px;">Ошибка обновления записи').fadeIn().fadeOut(4000, function() {
                    $(this).remove();
                });
            }
        });
    });
    function getRoles() {
        $.getJSON('http://localhost:8080/api/roleList', function (json) {
            var tr = [];
            for (var i = 0; i < json.length; i++) {
                tr.push('<tr>');
                tr.push('<td><p>' + json[i].id + '</p></td>');
                tr.push('<td><p>' + json[i].role + '</p></td>');


                tr.push('<td><button type="button" class="btn btn-success edit role">Редактировать</button>&nbsp;&nbsp;<button type="button" class="btn btn-danger delete role" id=' + json[i].id + '>Удалить</button></td>');
                tr.push('</tr>');
            }
            ;
            $('#right_admin').html('<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Добавить  роль</button> <table class="table shadow "><thead class="thead-dark"><tr><th  class="w-25">Id</th><th  class="w-25">Роль</th><th  class="w-25">Действия</th></tr></thead><tbody>'+tr.join('')+'</tbody></table>');

            $('.modal-body').html('<div class="form-group"> <label for="role">Роль</label><input type="text" class="form-control" id="role" name="role"></div>');

            $('.modal-title').html('Добавление роли');
        });
        $('#addNew').removeClass();
        $('#addNew').addClass('btn');
        $('#addNew').addClass('btn-primary');
        $('#addNew').addClass('btn-role');
    };
    $(document).delegate('.admin-roles', 'click', function(event) {
        event.preventDefault();
        getRoles();
    });
    $(document).delegate('.admin-userroleapp', 'click', function(event) {
        event.preventDefault();
        $('#right_admin').html("<div>Пока не работает</div>");
    });

    $(document).delegate('#addNew.btn-role', 'click', function(event) {
        event.preventDefault();

        var role = $('#role').val();

        var json = {
            "role" : role

        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "http://localhost:8080/api/role",
            data: JSON.stringify(json),
            cache: false,
            success: function() {
                $("#msg").html( "<span style='color: green'>Роль успешно добавлена</span>" );
                window.setTimeout(function(){getRoles(); $("#msg").html()},1000)
            },
            error: function(err) {
                $("#msg").html( "<span style='color: red'>Что-то пошло не так</span>" );
                window.setTimeout(function(){ $("#msg").html()},1000);
            }
        });
    });
    $(document).delegate('.edit.role', 'click', function() {
        var parent = $(this).parent().parent();
        var id = parent.children("td:nth-child(1)").children("p");
        var role = parent.children("td:nth-child(2)");
        var buttons = parent.children("td:nth-child(3)");

        role.html("<input type='text' class='form-control'   value='" + role.children("p").html() + "'/>");

        buttons.html('<button type="button" class="btn btn-success save role" ">Сохранить</button>&nbsp;&nbsp;<button type="button" class="btn btn-danger delete role" id=' + id.html() + '>Удалить</button>');
    });
    $(document).delegate('.delete.role', 'click', function() {
        if (confirm('Вы действительно хотите удалить запись?')) {
            var id = $(this).attr('id');
            var parent = $(this).parent().parent();
            $.ajax({
                type: "DELETE",
                url: "http://localhost:8080/api/role/" + id,
                cache: false,
                success: function() {
                    parent.fadeOut('slow', function() {
                        $(this).remove();
                    });
                    getRoles()
                },
                error: function() {
                    $('#err').html('<span style=\'color:red; font-weight: bold; font-size: 30px;\'>Ошибка удаления записи').fadeIn().fadeOut(4000, function() {
                        $(this).remove();
                    });
                }
            });
        }
    });

    $(document).delegate('.save.role', 'click', function() {
        var parent = $(this).parent().parent();

        var id = parent.children("td:nth-child(1)").children("p");
        var role = parent.children("td:nth-child(2)");
        var buttons = parent.children("td:nth-child(3)");

        $.ajax({
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            url: "http://localhost:8080/api/role",
            data: JSON.stringify({'id' : id.html(), 'role' : role.children("input[type=text]").val() }),
            cache: false,
            success: function() {
                role.html('<p>'+role.children("input[type=text]").val()+'</p>');
                buttons.html('<button type="button" class="btn btn-success edit role" id="' + id.html() + '">Редактировать</button>&nbsp;&nbsp;<button type="button" class="btn btn-danger delete role" id=' + id.html() + '>Удалить</button>');
            },
            error: function() {
                $('#err').html('<span style=\'color:red; font-weight: bold; font-size: 30px;\'>Ошибка обновления записи').fadeIn().fadeOut(4000, function() {
                    $(this).remove();
                });
            }
        });
    });
    function getApplications() {
        $.getJSON('http://localhost:8080/api/applicationList', function (json) {
            var tr = [];
            for (var i = 0; i < json.length; i++) {
                tr.push('<tr>');
                tr.push('<td><p>' + json[i].id + '</p></td>');
                tr.push('<td><p>' + json[i].domain + '</p></td>');


                tr.push('<td><button type="button" class="btn btn-success edit application">Редактировать</button>&nbsp;&nbsp;<button type="button" class="btn btn-danger delete application" id=' + json[i].id + '>Удалить</button></td>');
                tr.push('</tr>');
            }
            ;
            $('#right_admin').html('<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Добавить  приложение</button> <table class="table shadow "><thead class="thead-dark"><tr><th  class="w-25">Id</th><th  class="w-25">Домен</th><th  class="w-25">Действия</th></tr></thead><tbody>'+tr.join('')+'</tbody></table>');

            $('.modal-body').html('<div class="form-group"> <label for="domain">Домен</label><input type="text" class="form-control" id="domain" name="domain"></div>');

            $('.modal-title').html('Добавление приложения');
        });
        $('#addNew').removeClass();
        $('#addNew').addClass('btn');
        $('#addNew').addClass('btn-primary');
        $('#addNew').addClass('btn-application');
    };
    $(document).delegate('.admin-applications', 'click', function(event) {
        event.preventDefault();
        getApplications();
    });


    $(document).delegate('#addNew.btn-application', 'click', function(event) {
        event.preventDefault();

        var domain = $('#domain').val();

        var json = {
            "domain" : domain

        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "http://localhost:8080/api/application",
            data: JSON.stringify(json),
            cache: false,
            success: function() {
                $("#msg").html( "<span style='color: green'>Приложение успешно добавлено</span>" );
                window.setTimeout(function(){getApplications(); $("#msg").html()},1000)
            },
            error: function(err) {
                $("#msg").html( "<span style='color: red'>Что-то пошло не так</span>" );
                window.setTimeout(function(){ $("#msg").html()},1000);
            }
        });
    });
    $(document).delegate('.edit.application', 'click', function() {
        var parent = $(this).parent().parent();
        var id = parent.children("td:nth-child(1)").children("p");
        var domain = parent.children("td:nth-child(2)");
        var buttons = parent.children("td:nth-child(3)");

        domain.html("<input type='text' class='form-control'   value='" + domain.children("p").html() + "'/>");

        buttons.html('<button type="button" class="btn btn-success save application" ">Сохранить</button>&nbsp;&nbsp;<button type="button" class="btn btn-danger delete application" id=' + id.html() + '>Удалить</button>');
    });
    $(document).delegate('.delete.application', 'click', function() {
        if (confirm('Вы действительно хотите удалить запись?')) {
            var id = $(this).attr('id');
            var parent = $(this).parent().parent();
            $.ajax({
                type: "DELETE",
                url: "http://localhost:8080/api/application/" + id,
                cache: false,
                success: function() {
                    parent.fadeOut('slow', function() {
                        $(this).remove();
                    });
                    getApplications()
                },
                error: function() {
                    $('#err').html('<span style=\'color:red; font-weight: bold; font-size: 30px;\'>Ошибка удаления записи').fadeIn().fadeOut(4000, function() {
                        $(this).remove();
                    });
                }
            });
        }
    });

    $(document).delegate('.save.application', 'click', function() {
        var parent = $(this).parent().parent();

        var id = parent.children("td:nth-child(1)").children("p");
        var domain = parent.children("td:nth-child(2)");
        var buttons = parent.children("td:nth-child(3)");

        $.ajax({
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            url: "http://localhost:8080/api/application",
            data: JSON.stringify({'id' : id.html(), 'domain' : domain.children("input[type=text]").val() }),
            cache: false,
            success: function() {
                domain.html('<p>'+domain.children("input[type=text]").val()+'</p>');
                buttons.html('<button type="button" class="btn btn-success edit application" id="' + id.html() + '">Редактировать</button>&nbsp;&nbsp;<button type="button" class="btn btn-danger delete application" id=' + id.html() + '>Удалить</button>');
            },
            error: function() {
                $('#err').html('<span style=\'color:red; font-weight: bold; font-size: 30px;\'>Ошибка обновления записи').fadeIn().fadeOut(4000, function() {
                    $(this).remove();
                });
            }
        });
    });
</script>
    </html>