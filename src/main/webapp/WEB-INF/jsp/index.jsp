<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!doctype html>
<html>
<head>
<%@ include file="parts/meta.jsp" %>
<title>Главная</title>
<%@ include file="parts/head.jsp" %>
</head>
<body>
<%@ include file="parts/header.jsp" %>
<div class="container-fluid">
    <h2><a href="registration" >Зарегестрироваться</a></h2>
    <h2><a href="login" >Войти</a></h2>
    <h2><a href="logintoken" >Войти по токену</a></h2>
    <h2><a href="admin" >Админка</a></h2>
</div>
</body>
</html>