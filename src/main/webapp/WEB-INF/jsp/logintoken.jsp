<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <%@ include file="parts/meta.jsp" %>
    <title>Войти</title>
    <%@ include file="parts/head.jsp" %>
</head>

<body>

<div class="container-fluid">
    <jsp:include page="parts/header.jsp"></jsp:include>

    <div>
        <form id="loginform" method="POST" >
            <h2>Вход в систему</h2>
            <div>
                <div class="form-group">
                    <label for="token">Токен</label>
                    <input id="token" type="text" class="form-control" name="token"  placeholder="token"  autofocus="true"/>
                </div>

                <div class="form-group">
                    <label for="app">Приложение</label>
                    <input id="app" class="form-control"  name="app" type="text" placeholder="app"/>
                </div>
                <div id='msg'></div>
                <button type="submit" class="btn btn-primary" >Войти</button>

            </div>
        </form>

        <script>

            $("#loginform").submit(function(e) {

                e.preventDefault(); // avoid to execute the actual submit of the form.


                var token = $('#token').val();
                var app = $('#app').val();

                var json = {
                    "token" : token,
                    "app":app
                };
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "http://localhost:8080/api/logintoken",
                    data: JSON.stringify(json),
                    cache: false,
                    success: function(data) {
                        if (data == "OK") {


                            $("#msg").html("<span style='color: red'>" + data + "</span>");
                        }
                   else {
                            $("#msg").html("<span style='color: red'>" + data + "</span>");
                        }
                    },
                    error: function(err) {
                        $("#msg").html( "<span style='color: red'>Что-то пошло не так</span>" );
                        window.setTimeout(function(){ $("#msg").html()},1000);
                    }
                });
            });
        </script>
    </div>
</div>
</body>
</html>
