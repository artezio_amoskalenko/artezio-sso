<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <%@ include file="parts/meta.jsp" %>
    <title>Регистрация</title>
    <%@ include file="parts/head.jsp" %>
</head>

<body>

<div class="container-fluid">
    <jsp:include page="parts/header.jsp"></jsp:include>

    <form id="add" method="POST">
        <h2>Регистрация</h2>

        <div class="form-group">
            <label for="login">Логин</label>
            <input type="text" id="login" class="form-control" path="login" placeholder="Login" autofocus="true"></input>

        </div>
        <div class="form-group">
            <label for="password">Пароль</label>

            <input  id="password" type="password" class="form-control"  path="password" placeholder="Password">

            </input>
        </div>

        <div id='msg'></div>
        <button type="submit" class="btn btn-primary" >Зарегистрироваться</button>
    </form>
    <a href="/">Главная</a>
    <script>

        $("#add").submit(function(e) {

            e.preventDefault(); // avoid to execute the actual submit of the form.


            var name = $('#login').val();
            var password = $('#password').val();

            var json = {
                "login" : name,
                "password":password
            };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "http://localhost:8080/api/user",
                data: JSON.stringify(json),
                cache: false,
                success: function(data) {
                   if (data=="OK")
                   {
                    $("#msg").html( "<span style='color: green'>Пользователь успешно добавлен</span>" );
                    window.setTimeout(function(){getUsers(); $("#msg").html()},1000)

                    setTimeout(function() {
                        document.location.href="/";
                    }, 2000);
                   } else
                   {   $("#msg").html( "<span style='color: red'>"+data+"</span>" );
                       window.setTimeout(function(){ $("#msg").html()},1000);
                   }
                },
                error: function(err) {
                    $("#msg").html( "<span style='color: red'>Что-то пошло не так</span>" );
                    window.setTimeout(function(){ $("#msg").html()},1000);
                }
            });
        });
    </script>
</div>
</body>
</html>