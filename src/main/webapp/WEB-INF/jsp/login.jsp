<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <%@ include file="parts/meta.jsp" %>
    <title>Войти</title>
    <%@ include file="parts/head.jsp" %>
</head>

<body>

<div class="container-fluid">
    <jsp:include page="parts/header.jsp"></jsp:include>
    <% if (session.getAttribute("session")!=null){ out.print("Вы уже авторизованы, ваша сессия "+session.getAttribute("session")); } ;%>

    <div>
        <form id="loginform" method="POST" >
            <h2>Вход в систему</h2>
            <div>
                <div class="form-group">
                    <label for="login">Логин</label>
                    <input id="login" type="text" class="form-control" name="login"  placeholder="Login"  autofocus="true"/>
                </div>

                <div class="form-group">
                    <label for="password">Логин</label>
                    <input id="password" class="form-control"  name="password" type="password" placeholder="Password"/>
                </div>
                <div id='msg'></div>
                <button type="submit" class="btn btn-primary" >Войти</button>
                <h4><a href="/registration">Зарегистрироваться</a></h4>
            </div>
        </form>

        <script>

            $("#loginform").submit(function(e) {

                e.preventDefault(); // avoid to execute the actual submit of the form.


                var name = $('#login').val();
                var password = $('#password').val();

                var json = {
                    "login" : name,
                    "password":password
                };
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "http://localhost:8080/api/login",
                    data: JSON.stringify(json),
                    cache: false,
                    success: function(data) {
                        if (data == "OK") {


                            $("#msg").html("<span style='color: red'>" + data + "</span>");
                        }
                   else {
                            $("#msg").html("<span style='color: red'>" + data + "</span>");
                        }
                    },
                    error: function(err) {
                        $("#msg").html( "<span style='color: red'>Что-то пошло не так</span>" );
                        window.setTimeout(function(){ $("#msg").html()},1000);
                    }
                });
            });
        </script>
    </div>
</div>
</body>
</html>
