<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
 <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
  
<!doctype html>
<html>
<head>
<%@ include file="parts/meta.jsp" %>  
<title>Главная</title>
<%@ include file="parts/head.jsp" %>  
</head>
<body>

<div class="container">
<jsp:include page="parts/header.jsp"></jsp:include>

 <h2>Добро пожаловать, <a href="/login">войдите</a> или <a href="/registration">зарегистрируйтесь</a></h2> </sec:authorize>
</div>
</body>
</html>