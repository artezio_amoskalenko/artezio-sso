CREATE TABLE Roles (
  id           int(11)   NOT NULL,
  role         VARCHAR(40),
  PRIMARY KEY (id)
);
