CREATE TABLE Sessions (
  id       int(11)   NOT NULL,
  token    VARCHAR(255),
  ttl      TIMESTAMP ,
  PRIMARY KEY (id),
  FOREIGN KEY (id)  REFERENCES Users (id)  ON DELETE CASCADE
);
