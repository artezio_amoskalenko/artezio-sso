CREATE TABLE Roles (
  id           int(11)   AUTO_INCREMENT,
  role         VARCHAR(40),
  PRIMARY KEY (id)
);
