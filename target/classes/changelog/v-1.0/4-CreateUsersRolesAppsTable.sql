CREATE TABLE UsersRolesApps (
  id_user            int(11)   NOT NULL,
  id_role            int(11)   NOT NULL,
  id_app             int(11)   NOT NULL,
  FOREIGN KEY (id_user)  REFERENCES Users (id),
  FOREIGN KEY (id_role)  REFERENCES Roles (id),
  FOREIGN KEY (id_app)   REFERENCES Applications (id)
);
